package rose.calculaterosa;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalculateActivity extends AppCompatActivity {
//public class CalculateActivity extends AppCompatActivity implements View.OnClickListener {
    TextView result;
    TextView show;
    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnAC;
    Button btnPercent;
    Button btnSign;
    Button btnDiv;
    Button btnMulti;
    Button btnSub;
    Button btnAdd;
    Button btnDot;
    Button btnEqual;
    float num1;
    float num2;
    String OP = "";
    boolean flag = false;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_calculate);
//        findViewById(R.id.btn0).setOnClickListener(this);
//        findViewById(R.id.btn1).setOnClickListener(this);
//        findViewById(R.id.btn2).setOnClickListener(this);
//        findViewById(R.id.btn3).setOnClickListener(this);
//        findViewById(R.id.btn4).setOnClickListener(this);
//        findViewById(R.id.btn5).setOnClickListener(this);
//        findViewById(R.id.btn6).setOnClickListener(this);
//        findViewById(R.id.btn7).setOnClickListener(this);
//        findViewById(R.id.btn8).setOnClickListener(this);
//        findViewById(R.id.btn9).setOnClickListener(this);
//        findViewById(R.id.result).setOnClickListener(this);
//        findViewById(R.id.show).setOnClickListener(this);
//        findViewById(R.id.btnAC).setOnClickListener(this);
//        findViewById(R.id.btnSign).setOnClickListener(this);
//        findViewById(R.id.btnDiv).setOnClickListener(this);
//        findViewById(R.id.btnMulti).setOnClickListener(this);
//        findViewById(R.id.btnSub).setOnClickListener(this);
//        findViewById(R.id.btnAdd).setOnClickListener(this);
//        findViewById(R.id.btnEqual).setOnClickListener(this);
//        findViewById(R.id.btnDot).setOnClickListener(this);
//        findViewById(R.id.btnPercent).setOnClickListener(this);
//    }
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);
        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);

        btnAC = findViewById(R.id.btnAC);
        btnPercent = findViewById(R.id.btnPercent);
        btnSign = findViewById(R.id.btnSign);
        btnDiv = findViewById(R.id.btnDiv);
        btnMulti = findViewById(R.id.btnMulti);
        btnSub = findViewById(R.id.btnSub);
        btnAdd = findViewById(R.id.btnAdd);
        btnDot = findViewById(R.id.btnDot);
        btnEqual = findViewById(R.id.btnEqual);
        result = findViewById(R.id.result);
        show = findViewById(R.id.show);


    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn0) {
            String tmp = result.getText().toString().trim();
            if (!tmp.equalsIgnoreCase("0.0")) {
                result.append("0");
            }
        }
        else if (id == R.id.btn1)
            setNum(1);
        else if (id == R.id.btn2)
            setNum(2);
        else if (id == R.id.btn3)
            setNum(3);
        else if (id == R.id.btn4)
            setNum(4);
        else if (id == R.id.btn5)
            setNum(5);
        else if (id == R.id.btn6)
            setNum(6);
        else if (id == R.id.btn7)
            setNum(7);
        else if (id == R.id.btn8)
            setNum(8);
        else if (id == R.id.btn9)
            setNum(9);
        else if (id == R.id.btnAdd) {
            num1 = Float.parseFloat(result.getText().toString());
            OP = "+";
        } else if (id == R.id.btnSub) {
            num1 = Float.parseFloat(result.getText().toString());
            OP = "-";
        } else if (id == R.id.btnMulti) {
            num1 = Float.parseFloat(result.getText().toString());
            OP = "*";
        } else if (id == R.id.btnDiv) {
            num1 = Float.parseFloat(result.getText().toString());
            OP = "/";
        } else if (id == R.id.btnEqual) {
            num2 = Float.parseFloat(result.getText().toString());
            float result = 0;
            if (OP == "+")
                result = num1 + num2;
            else if (OP == "-")
                result = num1 - num2;
            else if (OP == "x")
                result = num1 * num2;
            else if (OP == "/")
                result = num1 / num2;
            num1 = num2 = 0;
            OP = "";
        } else if (id == R.id.btnAC) {
            num1 = num2 = 0;
            OP = "";
            result.setText("0.0");
            flag = false;
        } else if (id == R.id.btnSign) {
            float NUM = Float.parseFloat(result.getText().toString());
            if (NUM != 0)
                result.setText(( NUM * -1 ) + "");
        }
    }
    void setNum(int num) {
        String tmp = result.getText().toString();
        if (OP == "") {
            if (tmp.equalsIgnoreCase("0.0"))
                result.setText(num + "");
            else
                result.setText(tmp + num + "");
        } else {
            if (flag == false) {
                result.setText(num + "");
                flag = true;
            } else
                result.setText(tmp + num);
        }
    }
}
